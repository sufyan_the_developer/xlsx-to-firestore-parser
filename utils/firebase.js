var firebase = require('firebase')

const firebaseConfig = null

if (!firebaseConfig) {
  throw new Error('Firebase config not set')
}

firebase.initializeApp(firebaseConfig)

module.exports = firebase.firestore()
