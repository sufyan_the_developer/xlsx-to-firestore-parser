var parser = new (require('simple-excel-to-json').XlsParser)()
var db = require('./firebase.js')

function parse(filePath = null, collection = null, idField = null) {
    try {
        if (filePath === null) {
            throw new Error(`No xlsx file for ${collection}-${idField}`)
        }
        var doc = parser.parseXls2Json(filePath, { isNested: true })
        
        if (collection === null) {
            throw new Error('Firestore DB path not set')
        }
        
        // Get first data
        const data = doc[0]

        const collectionRef = db.collection(collection)
        let promises = null

        // Iterate on data and upload data on firestore
        if (idField) {
            promises = data.map((doc, index) => collectionRef.doc(`${doc[idField]}`).set(doc))
        } else {
            promises = data.map((doc, index) => collectionRef.doc(`${index}`).set(doc))
        }
        
        Promise.all(promises)
        console.log(`\n${collection} - ${idField} -> Done writing records\n`)
        return true
    } catch (error) {
        console.log(`\n${collection} - ${idField} -> Error writing records\n`)
        console.log(error)
        return false
    }   
}

module.exports = parse
