# Xlsx To Firestore Parser

# Configuration
Add firebase config in firebase.js file.

```javascript
const firebaseConfig = {
  apiKey: "..........",
  authDomain: "..........",
  databaseURL: "..........",
  projectId: "..........",
  storageBucket: "..........",
  messagingSenderId: "..........",
  appId: ".........."
}
```

# Steps to use parser
1. Select formatted Xlsx file.
2. Enter collection name in which data needs to be filled.
3. Enter id field optional in case if data needs to be iterated in sepcific id(key/column) from Xlsx data.
