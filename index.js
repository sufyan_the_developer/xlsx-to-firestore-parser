var express = require('express')
var app = express()
var parse = require('./utils/parser.js')
const fs = require('fs')
var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })

app.get('/', function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'})
  res.write('<br><h1>Welcome to xlsx to Firebase Parser!</h1><br>')
  res.write('<button type="button" onclick="window.location.href=\'/select-data\'"><h3>Start!</h3></button>')
  res.end()
})

app.get('/select-data', function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'})
  res.write('<form action="/upload-data" method="post" enctype="multipart/form-data">')
  res.write('<input type="file" accept=".xlsx" name="xlsxFileToUpload"><br>')
  res.write('<input type="text" name="collectionName" placeholder="Please enter collection name"><br>')
  res.write('<input type="text" name="idFieldName" placeholder="Please enter id field name">\toptional<br>')
  res.write('<input type="submit">')
  res.write('</form>')

  res.end()
})

app.post('/upload-data', upload.single('xlsxFileToUpload'), function (req, res, next) {
  if (req.file && req.file.path) {
    const path = req.file.path
    const { collectionName, idFieldName } = req.body
    if (collectionName) {
      let parseAndUploadDone = false
      if (idFieldName) {
        parseAndUploadDone = parse(path, collectionName, idFieldName)
      } else {
        parseAndUploadDone = parse(path, collectionName)
      }

      if (parseAndUploadDone) {
        res.writeHead(200, {'Content-Type': 'text/html'})
        res.write('<br><h1>Done writing records</h1><br>')
      } else {
        res.writeHead(400, {'Content-Type': 'text/html'})
        res.write('<br><h1>Error in writing records</h1><br>')
      }
      res.write('<button type="button" onclick="window.location.href=\'/select-data\'">Go back!</button>')
      res.end()

      // After task done delete file
      try {
        fs.unlinkSync(path)
        //file removed
      } catch(err) {
        console.error(err)
      }
    } else {
      res.writeHead(400, {'Content-Type': 'text/html'})
      res.write('<br><h1>Please enter collection name!</h1><br>')
      res.write('<button type="button" onclick="window.location.href=\'/select-data\'">Go back!</button>')
      res.end()
    }
  } else {
    res.writeHead(400, {'Content-Type': 'text/html'})
    res.write('<br><h1>Please upload xlsx file!</h1><br>')
    res.write('<button type="button" onclick="window.location.href=\'/select-data\'">Go back!</button>')
    res.end()
  }
})

app.listen(3000, function () {
  console.log('Firebase Parser Tool App listening on port 3000!')
})
